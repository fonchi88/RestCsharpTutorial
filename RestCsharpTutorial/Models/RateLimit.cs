﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestCsharpTutorial.Models
{
    public class RateLimit
    {
        public int limit { get; set; }
        public int remaining { get; set; }
        public int reset { get; set; }
    }
}
