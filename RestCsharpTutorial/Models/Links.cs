﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestCsharpTutorial.Models
{
    public class Links
    {
        public Self self { get; set; }
        public Edit edit { get; set; }
        public Avatar avatar { get; set; }
    }
}
