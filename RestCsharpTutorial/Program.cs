﻿using System;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Authenticators;
using RestCsharpTutorial.Models;
using System.Linq;

namespace RestCsharpTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            //var client = new RestClient("https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=2020-08-08");
            //client.Timeout = -1;
            //var request = new RestRequest(Method.GET);
            //IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.ResponseStatus);
            RestClient client = new RestClient();
            Uri baseUrl;
            try {
                //var client = new RestClient("https://gorest.co.in/public-api/users");
                baseUrl = new Uri("https://gorest.co.in");
                client.BaseUrl = baseUrl;
                client.Timeout = -1;
                client.Authenticator = new OAuth2AuthorizationRequestHeaderAuthenticator("gz9TRJCwDWJBYq-8wEcuHiw-Reoz7NcLqTKN", "Bearer");
                var request = new RestRequest("public-api/users", Method.GET);
                //request.AddHeader("Authorization", "Bearer gz9TRJCwDWJBYq-8wEcuHiw-Reoz7NcLqTKN");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Cookie", "_csrf=78d60764f1a476f48169d20544d89838e78a8a0ef85edbe6068c258a1fe344dba%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22EuLh_29lUFSptLRvooQamf4ZigrlzPgb%22%3B%7D");
                IRestResponse response = client.Execute(request);
                Users responseContent = JsonConvert.DeserializeObject<Users>(response.Content);
                Console.WriteLine(responseContent.result[0].id);
            }
            catch(Exception e)
            {

                Console.WriteLine(e);
            }

        }
    }
}
